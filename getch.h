#ifndef GETCH_H_INCLUDED
#define GETHC_H_INCLUDED

#include <stdio.h>
#include <unistd.h>
#include <termios.h>

char getch ();

#endif // GETHC_H_INCLUDED
