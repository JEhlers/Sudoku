#include <iostream>
#include "getch.h"

void Input (short Toll[][9][10]);
short Print (short Toll[][9][10],short x, short y);
short Print (short Toll[][9][10]);
void PrintAll (short Toll[][9][10]);
short PrintRow (short Toll[][9][10], short i, short j);
short PrintCal (short Toll[][9][10], short j, short i);
short PrintGrid (short Toll[][9][10], short k, short p);
void DEL (short Toll[][9][10],short i, short j);
void DEL (short Toll[][9][10],short i, short j, short l);
bool Solve (short Toll[][9][10]); //2.!
bool TestRow(short Toll[][9][10], short i, short l);
bool TestCal(short Toll[][9][10], short j, short l);
bool TestGrid(short Toll[][9][10], short k, short l);


int main(int argc, char **argv) {
    
    short Toll[9][9][10];
    
    for (short i = 0;i < 9; i++) {
        for (short j = 0;j < 9;j++) {
            for (short l = 0;l < 9;l++) {
                Toll[i][j][l] = l+1;
            }
            Toll[i][j][9] = 0;
        }
    }
    if (argc >= 2) {
        std::string S = argv[1];
        if (S == "1") {
            std::cout << "!";
            Toll[0][0][9] = 5;
            Toll[0][3][9] = 3;
            Toll[0][4][9] = 8;
            Toll[0][5][9] = 9;
            
            Toll[1][0][9] = 3;
            Toll[1][1][9] = 9;
            Toll[1][2][9] = 2;
            Toll[1][5][9] = 6;
            Toll[1][6][9] = 1;
            
            Toll[2][4][9] = 4;
            Toll[2][6][9] = 9;
            Toll[2][8][9] = 5;
            
            Toll[3][0][9] = 8;
            Toll[3][1][9] = 3;
            Toll[3][2][9] = 1;
            Toll[3][5][9] = 7;
            
            Toll[4][2][9] = 7;
            Toll[4][4][9] = 5;
            Toll[4][6][9] = 4;
            Toll[4][7][9] = 6;
            Toll[4][8][9] = 1;
            
            Toll[5][0][9] = 4;
            Toll[5][1][9] = 6;
            Toll[5][3][9] = 2;
            Toll[5][4][9] = 9;
            Toll[5][6][9] = 8;
            Toll[5][7][9] = 7;
            
            Toll[6][0][9] = 7;
            Toll[6][1][9] = 4;
            Toll[6][3][9] = 9;
            Toll[6][4][9] = 2;
            Toll[6][6][9] = 3;
            
            Toll[7][0][9] = 2;
            Toll[7][1][9] = 8;
            
            Toll[8][4][9] = 3;
            Toll[8][6][9] = 6;
            Toll[8][7][9] = 4;
        } else if (S == "2") {
            Toll[0][0][9] = 2;
            Toll[0][1][9] = 9;
            Toll[0][4][9] = 7;
            
            Toll[1][2][9] = 5;
            Toll[1][3][9] = 9;
            Toll[1][6][9] = 7;
            
            Toll[2][8][9] = 6;
            
            Toll[3][2][9] = 6;
            Toll[3][6][9] = 8;
            Toll[3][7][9] = 4;
            
            Toll[4][1][9] = 8;
            Toll[4][4][9] = 5;
            Toll[4][5][9] = 1;
            Toll[4][8][9] = 7;
            
            Toll[5][1][9] = 1;
            Toll[5][2][9] = 9;
            Toll[5][5][9] = 4;
            Toll[5][7][9] = 6;
            
            Toll[6][0][9] = 3;
            Toll[6][1][9] = 7;
            Toll[6][3][9] = 1;
            
            Toll[7][7][9] = 8;
            
            Toll[8][0][9] = 6;
            Toll[8][6][9] = 5;
        } else if (S == "3") {
            Toll[0][0][9] = 5;
            Toll[0][2][9] = 4;
            Toll[0][3][9] = 2;
            Toll[0][8][9] = 1;
            
            Toll[1][1][9] = 2;
            Toll[1][5][9] = 1;
            Toll[1][7][9] = 6;
            
            Toll[2][0][9] = 8;
            Toll[2][3][9] = 5;
            
            Toll[3][0][9] = 3;
            Toll[3][2][9] = 2;
            Toll[3][7][9] = 9;
            
            Toll[4][4][9] = 7;
            
            Toll[5][1][9] = 4;
            Toll[5][6][9] = 5;
            Toll[5][8][9] = 3;
            
            Toll[6][5][9] = 7;
            Toll[6][8][9] = 8;
            
            Toll[7][1][9] = 7;
            Toll[7][3][9] = 3;
            Toll[7][7][9] = 5;
            
            Toll[8][0][9] = 1;
            Toll[8][5][9] = 6;
            Toll[8][6][9] = 7;
            Toll[8][8][9] = 4;
        }
        for (short i = 0;i < 9;i++) { // Reihe
            for (short j = 0;j < 9;j++) { // Spalte
                if (Toll[i][j][9] != 0) { // Wenn Zahl eingetragen
                    DEL(Toll,i,j); // Attribute Löschen
                }
            }
        }
    } else {
        Input(Toll);
    }
    system("clear");
    Print(Toll);
    if (!Solve(Toll)) {
        PrintAll(Toll);
    }
    return 0;
}
void Input (short Toll[][9][10]) {
    char C; // getch var
    short x = 0,y = 0; // Koordinaten
    do {
        system("clear"); // Clear
        Print(Toll,x,y); // Ausgeben
        
        std::cout << " X = " << x << " Y = " << y << " C = " << C << "\n"; // Koordinaten Ausgeben
        
        C = getch(); // Key Input
        
        if (C == 'w' && x > 0) { // Wenn W X--
            x--;
        } else if (C == 's' && x < 8) { // Wenn S X++
            x++;
        } else if (C == 'a' && y > 0) { // Wenn A Y--
            y--;
        } else if (C == 'd' && y < 8) { // Wenn D Y++
            y++;
        } else if (C >= 48 && C <= 57) { // Wenn Zahl 0-9
            Toll[x][y][9] = C-48; // Zahl eintragen
        }
        
    } while (C != 10); // Solgange nicht ENTER
    for (short i = 0;i < 9;i++) { // Reihe
        for (short j = 0;j < 9;j++) { // Spalte
            if (Toll[i][j][9] != 0) { // Wenn Zahl eingetragen
                DEL(Toll,i,j); // Attribute Löschen
            }
        }
    }
    
    //system("clear"); // Clear
}
short Print (short Toll[][9][10],short x, short y) {
    short Z = 0;
    for (short i = 0;i < 9; i++) {
        if (i == 0 || i == 3 || i == 6) std::cout << "|---------|---------|---------|\n";
        for (short j = 0;j < 9;j++) {
            if (j == 0 || j == 3 || j == 6) std::cout << "|";
            if (i == x && j == y) {
                std::cout << " " << "#" << " ";
            } else if (Toll[i][j][9] == 0) {
                std::cout << " " << "\033[30m" << Toll[i][j][9] << "\033[0m" << " ";
                Z++;
            } else {
                std::cout << " " << Toll[i][j][9] << " ";
            }
        }
        std::cout << "|\n";
    }
    std::cout << "|---------|---------|---------|\n";
    return Z;
}
short Print (short Toll[][9][10]) {
    short Z = 0;
    for (short i = 0;i < 9; i++) {
        if (i == 0 || i == 3 || i == 6) std::cout << "|---------|---------|---------|\n";
        for (short j = 0;j < 9;j++) {
            if (j == 0 || j == 3 || j == 6) std::cout << "|";
            if (Toll[i][j][9] == 0) {
                std::cout << " " << "\033[30m" << Toll[i][j][9] << "\033[0m" << " ";
            } else {
                std::cout << " " << Toll[i][j][9] << " ";
                Z++;
            }
        }
        std::cout << "|\n";
    }
    std::cout << "|---------|---------|---------|\n";
    std::cout << Z;
    return Z;
}
void PrintAll (short Toll[][9][10]) {
    std::cout << "\n"
              << "|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n"
              << "|   |   |   Row   |   |   |   |   | Column  |   |   |   |   |   Grid  |   |\n"
              << "|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n";
    for (short k = 0;k < 9;k++) {
        std::cout << "\n|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n";
        for (short p = 0;p < 9;p++) {
            int Z = 0;
            Z = PrintRow (Toll,k,p);
            if ( Z == 0) {
                std::cout << "| " << "\033[1;32m" << Z << "\033[0m" << " |   ";
            } else {
                std::cout << "| " << "\033[1;31m" << Z << "\033[0m" << " |   ";
            }
            Z = PrintCal (Toll,k,p);
            if (Z == 0) {
                std::cout << "| " << "\033[1;32m" << Z << "\033[0m" << " |   ";
            } else {
                std::cout << "| " << "\033[1;31m" << Z << "\033[0m" << " |   ";
            }
            Z = PrintGrid (Toll,k,p);
            if (Z == 0) {
                std::cout << "| " << "\033[1;32m" << Z << "\033[0m" << " |\n";
            } else {
                std::cout << "| " << "\033[1;31m" << Z << "\033[0m" << " |\n";
            }
        }
        std::cout << "|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n";
    }
}
short PrintRow (short Toll[][9][10], short i, short j) {
    std::cout << "| "<< i << " | " << j << " |";
    short Z = 0;
    for (short l = 0;l < 9;l++) {
        if (Toll[i][j][l] == 0) {
            std::cout << "\033[30m" << Toll[i][j][l] << "\033[0m";
        } else {
            std::cout << Toll[i][j][l];
            Z++;
        }
    }
    return Z;
}
short PrintCal (short Toll[][9][10], short j, short i) {
    std::cout << "| "<< i << " | " << j << " |";
    short Z = 0;
    for (short l = 0;l < 9;l++) {
        if (Toll[i][j][l] == 0) {
            std::cout << "\033[30m" << Toll[i][j][l] << "\033[0m";
        } else {
            std::cout << Toll[i][j][l];
            Z++;
        }
    }
    return Z;
}
short PrintGrid (short Toll[][9][10], short k, short p) {
    std::cout << "| "<< p/3+(k/3)*3 << " | " << p%3+(k%3)*3 << " |";
    short Z = 0;
    for (short l = 0;l < 9;l++) {
        if (Toll[p/3+(k/3)*3][p%3+(k%3)*3][l] == 0) {
            std::cout << "\033[30m" << Toll[p/3+(k/3)*3][p%3+(k%3)*3][l]<< "\033[0m";
        } else {
            std::cout << Toll[p/3+(k/3)*3][p%3+(k%3)*3][l];
            Z++;
        }
    }
    return Z;
}
bool TestRow(short Toll[][9][10], short i, short l) {
    short Z = 0;
    for (short j = 0;j < 9;j++) {
        if (Toll[i][j][l] == l+1) {
            Z++;
        }
    }
    if (Z >= 2 || Z == 0) {
        return false;
    } else if (Z == 1) {
        for (short j = 0;j < 9;j++) {
            if(Toll[i][j][l] == l+1) {
                Toll[i][j][9] = l+1;
                DEL(Toll,i,j);
                break;
            }
        }
    }
    return true;
}
bool TestCal(short Toll[][9][10], short j, short l) {
    short Z = 0;
    for (short i = 0;i < 9;i++) {
        if (Toll[i][j][l] == l+1) {
            Z++;
        }
        if (Z >= 2) return false;
    }
    if (Z == 0) return false;
    for (short i = 0;i < 9;i++) {
        if(Toll[i][j][l] == l+1) {
            Toll[i][j][9] = l+1;
            DEL(Toll,i,j);
            break;
        }
    }
    return true;
}
bool TestGrid(short Toll[][9][10], short k, short l) {
    short Z = 0;
    for (short o = 0;o < 9;o++) {
        if (Toll[o/3+(k/3)*3][o%3+(k%3)*3][l] == l+1) {
            Z++;
            if (Z >= 2) return false;
        }
    }
    if (Z == 0) return false;
    for (short o = 0;o < 9;o++) {
        if(Toll[o/3+(k/3)*3][o%3+(k%3)*3][l] == l+1) {
            Toll[o/3+(k/3)*3][o%3+(k%3)*3][9] = l+1;
            DEL(Toll,o/3+(k/3)*3,o%3+(k%3)*3);
            break;
        }
    }
    return true;
}
bool Solve (short Toll[][9][10]) {
    bool changed;
    short Z = 0;
    do {
        Z++;
        changed = false;
        for (short k = 0;k < 9;k++) {
            for (short l = 0; l < 9;l++) {
                if (TestRow(Toll,k,l)) changed = true;
                if (TestCal(Toll,k,l)) changed = true;
                if (TestGrid(Toll,k,l)) changed = true;
            }
        }
        /*if (changed == false) {
            int z = 0, u = 0;
            for (int i = 0;i < 9;i++) {
                for (int j = 0;j < 9;j++) {
                    if (Toll[i][j][9] == 0)z++;
                }
                if (z == 2) {
                    std::cout << "1 ";
                    for (int j = 0;j < 9;j++) {
                        if (Toll[i][j][9] == 0) {
                            std::cout << "1.2 ";
                            for (int k = j;k < 9;k++) {
                                if (Toll[i][k][9] == 0) {
                                    std::cout << "1.3 ";
                                    for (int l = 0;l < 9;l++) {
                                        if (Toll[i][j][l] == Toll[i][k][l] && Toll[i][k][l] != 0)u++;
                                    }
                                    if (u == 2) {
                                        std::cout << "2 ";
                                        for (int l = 0;l < 9;l++) {
                                            if (Toll[i][j][l] == Toll[i][k][l] && Toll[i][k][l] != 0) {
                                                for (int h = l;h < 9;h++) {
                                                    if (Toll[i][j][h] == Toll[i][k][h]) {
                                                        DEL(Toll,i,j,l);
                                                        DEL(Toll,i,j,h);
                                                        DEL(Toll,i,k,l);
                                                        DEL(Toll,i,k,h);
                                                        std::cout << "3 ! ";
                                                        changed = true;
                                                        break;
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }*/
    } while (changed);
    std::cout << Z << " Durchläufe\n\n";
    if (Print(Toll) == 81) {
        return true;
    } else {
        return false;
    }
}
void DEL (short Toll[][9][10],short i, short j) {
    for (short k = 0;k < 9;k++) { // Attribut Reihe auf 0
        Toll[i][k][Toll[i][j][9]-1] = 0;
    }
    for (short k = 0;k < 9;k++) { // Attribut Spalte auf 0
        Toll[k][j][Toll[i][j][9]-1] = 0;
    }
    for (short p = 0;p < 3;p++) { // Attribut Kasten auf 0
        for (short o = 0;o < 3;o++) {
            Toll[o+((i/3)*3)][p+((j/3)*3)][Toll[i][j][9]-1] = 0;
        }
    }
    for (short l = 0;l < 9;l++) { // Attribut Zahl auf 0
        Toll[i][j][l] = 0;
    }
}
void DEL (short Toll[][9][10],short i, short j, short l) {
    for (short k = 0;k < 9;k++) { // Attribut Reihe auf 0
        Toll[i][k][l] = 0;
    }
    for (short k = 0;k < 9;k++) { // Attribut Spalte auf 0
        Toll[k][j][l] = 0;
    }
    for (short p = 0;p < 3;p++) { // Attribut Kasten auf 0
        for (short o = 0;o < 3;o++) {
            Toll[o+((i/3)*3)][p+((j/3)*3)][l] = 0;
        }
    }
    for (short l = 0;l < 9;l++) { // Attribut Zahl auf 0
        Toll[i][j][l] = 0;
    }
}
