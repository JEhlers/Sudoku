#ifndef SUDOKU_H_INCLUDED
#define SUDOKU_H_INCLUDED

#include <iostream>
#include "getch.h"

/*
delgrid funkt nicht
*/

class Sudoku {
public:
   Sudoku(void);
   void reset (void);
   void input (void);
   void preset (short n);
   short print (short x, short y);
   short print (void);
   void printall (void);
   short printrow (short x, short y);
   short printcolumn (short y, short x);
   short printgrid (short x, short y);
   short printfield (short x, short y);
   bool testrow(short x, short n);
   bool testcolumn(short y, short n);
   bool testgrid(short k, short n);
   bool testfield(short x, short y);
   short solve (void);
   void set(short x,short y, short n);
   void set(short x,short y, short z, bool n);
   void insert(short x, short y, short z);
   short get(short x,short y);
   bool get(short x, short y, short z);
   void del(short x,short y);
   void del(short x,short y,short z);
   void delrow(short x, short z);
   void delcolumn(short y, short z);
   void delgrid(short x, short y, short z);
   void delfield(short x, short y);
private:
   short field[9][9];
   bool possible[9][9][9];
};

#endif // SUDOKU_H_INCLUDED
