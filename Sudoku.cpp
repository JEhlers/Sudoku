#include "Sudoku.h"

   Sudoku::Sudoku(void) {
      for (short i = 0;i < 9;i++) {
         for (short j = 0;j < 9;j++) {
            field[i][j] = 0;
            for (short l = 0;l < 9;l++) {
               possible[i][j][l] = true;
            }
         }
      }
   }
   void Sudoku::reset (void) {
      for (short i = 0;i < 9;i++) {
         for (short j = 0;j < 9;j++) {
            //field[i][j] = 0;
            for (short l = 0;l < 9;l++) {
               possible[i][j][l] = true;
            }
         }
      }
   }
   void Sudoku::input (void) {
       char C; // getch var
       short x = 0,y = 0; // Koordinaten
       do {
           system("clear"); // Clear
           print(x,y); // Ausgeben

           std::cout << " X = " << x << " Y = " << y << " C = " << C << "\n"; // Koordinaten Ausgeben

           C = getch(); // Key Input

           if (C == 'w' && x > 0) { // Wenn W X--
               x--;
           } else if (C == 's' && x < 8) { // Wenn S X++
               x++;
           } else if (C == 'a' && y > 0) { // Wenn A Y--
               y--;
           } else if (C == 'd' && y < 8) { // Wenn D Y++
               y++;
           } else if (C >= 48 && C <= 57) { // Wenn Zahl 0-9
               set(x,y,C-48); // Zahl eintragen
           }

       } while (C != 10); // Solgange nicht ENTER

       for (short i = 0;i < 9;i++) {
          for (short j = 0;j < 9;j++) {
             if (get(i,j) != 0) {
                del(i,j);
             }
          }
       }
       //system("clear"); // Clear
   }
   void Sudoku::preset (short n) {
      if (n == 1) {
         insert(0,0,5);
         insert(0,3,3);
         insert(0,4,8);
         insert(0,5,9);

         insert(1,0,3);
         insert(1,1,9);
         insert(1,5,6);
         insert(1,6,1);

         insert(2,4,4);
         insert(2,6,9);
         insert(2,8,5);

         insert(3,0,8);
         insert(3,1,3);
         insert(3,2,1);
         insert(3,5,7);

         insert(4,2,7);
         insert(4,4,5);
         insert(4,6,4);
         insert(4,7,6);
         insert(4,8,1);

         insert(5,0,4);
         insert(5,1,6);
         insert(5,3,2);
         insert(5,4,9);
         insert(5,6,8);
         insert(5,7,7);

         insert(6,0,7);
         insert(6,1,4);
         insert(6,3,9);
         insert(6,4,2);
         insert(6,6,3);

         insert(7,0,2);
         insert(7,1,8);

         insert(8,4,3);
         insert(8,6,6);
         insert(8,7,4);
      } else if (n == 2) {
         insert(0,0,2);
         insert(0,1,9);
         insert(0,4,7);

         insert(1,2,5);
         insert(1,3,9);
         insert(1,6,7);

         insert(2,8,6);

         insert(3,2,6);
         insert(3,6,8);
         insert(3,7,4);

         insert(4,1,8);
         insert(4,4,5);
         insert(4,5,1);
         insert(4,8,7);

         insert(5,1,1);
         insert(5,2,9);
         insert(5,5,4);
         insert(5,7,6);

         insert(6,0,3);
         insert(6,1,7);
         insert(6,3,1);

         insert(7,7,8);

         insert(8,0,6);
         insert(8,6,5);
      } else if (n == 3) {
         insert(0,0,5);
         insert(0,2,4);
         insert(0,3,2);
         insert(0,8,1);

         insert(1,1,2);
         insert(1,5,1);
         insert(1,7,6);

         insert(2,0,8);
         insert(2,3,5);

         insert(3,0,3);
         insert(3,2,2);
         insert(3,7,9);

         insert(4,4,7);

         insert(5,1,4);
         insert(5,6,5);
         insert(5,8,3);

         insert(6,5,7);
         insert(6,8,8);

         insert(7,1,7);
         insert(7,3,3);
         insert(7,7,5);

         insert(8,0,1);
         insert(8,5,6);
         insert(8,6,7);
         insert(8,8,4);
      } else if (n == 3) {

      }
   }
   short Sudoku::print (short x, short y) {
       short Z = 0;
       for (short i = 0;i < 9; i++) {
           if (i == 0 || i == 3 || i == 6) std::cout << "|---------|---------|---------|\n";
           for (short j = 0;j < 9;j++) {
               if (j == 0 || j == 3 || j == 6) std::cout << "|";
               if (i == x && j == y) {
                   std::cout << " " << "#" << " ";
               } else if (get(i,j) == 0) {
                   std::cout << " " << "\033[30m" << get(i,j) << "\033[0m" << " ";
                   Z++;
               } else {
                   std::cout << " " << get(i,j) << " ";
               }
           }
           std::cout << "|\n";
       }
       std::cout << "|---------|---------|---------|\n";
       std::cout << Z;
       return Z;
   }
   short Sudoku::print (void) {
       short Z = 0;
       for (short i = 0;i < 9; i++) {
           if (i == 0 || i == 3 || i == 6) std::cout << "|---------|---------|---------|\n";
           for (short j = 0;j < 9;j++) {
               if (j == 0 || j == 3 || j == 6) std::cout << "|";
               if (get(i,j) == 0) {
                   std::cout << " " << "\033[30m" << get(i,j) << "\033[0m" << " ";
               } else {
                   std::cout << " " << get(i,j) << " ";
                   Z++;
               }
           }
           std::cout << "|\n";
       }
       std::cout << "|---------|---------|---------|\n";
       std::cout << Z;
       return Z;
   }
   void Sudoku::printall (void) {
       std::cout << "\n"
                 << "|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n"
                 << "| X | Y |   Row   | N |   | X | Y | Column  | N |   | X | Y |   Grid  | N |\n"
                 << "|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n";
       for (short k = 0;k < 9;k++) {
           std::cout << "\n|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n";
           for (short p = 0;p < 9;p++) {
               int Z = 0;
               Z = printrow(k,p);
               if ( Z == 0) {
                   std::cout << "| " << "\033[1;32m" << get(k,p) << "\033[0m" << " |   ";
               } else {
                   std::cout << "| " << "\033[1;31m" << Z << "\033[0m" << " |   ";
               }
               Z = printcolumn(k,p);
               if (Z == 0) {
                   std::cout << "| " << "\033[1;32m" << get(p,k) << "\033[0m" << " |   ";
               } else {
                   std::cout << "| " << "\033[1;31m" << Z << "\033[0m" << " |   ";
               }
               Z = printgrid(k,p);
               if (Z == 0) {
                   std::cout << "| " << "\033[1;32m" << get(p/3+(k/3)*3,p%3+(k%3)*3) << "\033[0m" << " |\n";
               } else {
                   std::cout << "| " << "\033[1;31m" << Z << "\033[0m" << " |\n";
               }
           }
           std::cout << "|---|---|---------|---|   |---|---|---------|---|   |---|---|---------|---|\n";
       }
       std::cout << "\n";
   }
   short Sudoku::printrow (short x, short y) {
       std::cout << "| "<< x+1 << " | " << y+1 << " |";
       short Z = 0;
       for (short l = 0;l < 9;l++) {
           if (get(x,y,l) == false) {
               std::cout << "\033[30m" << "0" << "\033[0m";
           } else {
               std::cout << l+1;
               Z++;
           }
       }
       return Z;
   }
   short Sudoku::printcolumn (short y, short x) {
       std::cout << "| "<< x+1 << " | " << y+1 << " |";
       short Z = 0;
       for (short l = 0;l < 9;l++) {
          if (get(x,y,l) == false) {
              std::cout << "\033[30m" << "0" << "\033[0m";
          } else {
              std::cout << l+1;
              Z++;
          }
       }
       return Z;
   }
   short Sudoku::printgrid (short x, short y) {
       std::cout << "| "<< y/3+1+(x/3)*3 << " | " << y%3+1+(x%3)*3 << " |";
       short Z = 0;
       for (short l = 0;l < 9;l++) {
           if (get(y/3+(x/3)*3,y%3+(x%3)*3,l) == false) {
               std::cout << "\033[30m" << "0" << "\033[0m";
           } else {
               std::cout << l+1;
               Z++;
           }
       }
       return Z;
   }
   short Sudoku::printfield (short x, short y) {
       std::cout << "| "<< x << " | " << y << " |";
       short Z = 0;
       for (short l = 0;l < 9;l++) {
           if (get(x,y,l) == false) {
               std::cout << "\033[30m" << "0" << "\033[0m";
           } else {
               std::cout << l+1;
               Z++;
           }
       }
       return Z;
   }
   bool Sudoku::testrow(short x, short n) {
      short Z = 0;
      for (short j = 0;j < 9;j++) {
         if (get(x,j,n) == true) {
            Z++;
         }
         if (Z >= 2) return false;
      }
      if (Z == 0) return false;
      for (short j = 0;j < 9;j++) {
         if (get(x,j,n) == true) {
            insert(x,j,n+1);
            return true;
         }
      }
   }
   bool Sudoku::testcolumn(short y, short n) {
       short Z = 0;
       for (short i = 0;i < 9;i++) {
           if (get(i,y,n) == true) {
               Z++;
           }
           if (Z >= 2) return false;
       }
       if (Z == 0) return false;
       for (short i = 0;i < 9;i++) {
          if (get(i,y,n) == true) {
            insert(i,y,n+1);
            return true;
         }
       }

   }
   bool Sudoku::testgrid(short k, short n) {
      short Z = 0;
      for (short o = 0;o < 9;o++) {
         if (get(o/3+(k/3)*3,o%3+(k%3)*3,n) == true) {
            Z++;
            if (Z >= 2) return false;
         }
       }
       if (Z == 0) return false;
       for (short o = 0;o < 9;o++) {
         if (get(o/3+(k/3)*3,o%3+(k%3)*3,n) == true) {
            insert(o/3+(k/3)*3,o%3+(k%3)*3,n+1);
            return true;
         }
       }

   }
   bool Sudoku::testfield(short x, short y) {
      short Z = 0;
      for (short l = 0;l < 9;l++) {
         if (get(x,y,l) == true) {
            Z++;
         }
      }
      if (Z == 1) {
         short l = 0;
         while (l < 9 && get(x,y,l) != true)l++;
         insert(x,y,l+1);
         return true;
      }
      return false;
   }
   short Sudoku::solve (void) {
       bool changed;
       short Z = 0;
       do {
           Z++;
           changed = false;
           for (short k = 0;k < 9;k++) {
               for (short l = 0; l < 9;l++) {
                   if (testrow(k,l) == true) changed = true;
                   if (testcolumn(k,l) == true) changed = true;
                   if (testgrid(k,l) == true) changed = true;
               }
           }
           if (!changed) {
             for (short k = 0;k < 9 && changed != true;k++) {
                for (short l = 0; l < 9 && changed != true;l++) {
                  if (testfield(k,l) == true) changed = true;
               }
             }
          }
       } while (changed);
       return Z;
   }
   void Sudoku::set(short x,short y, short n) {
      field[x][y] = n;
   }
   void Sudoku::set(short x,short y, short z, bool n) {
      possible[x][y][z] = n;
   }
   void Sudoku::insert(short x, short y, short z) {
      set(x,y,z);
      del(x,y);
   }
   short Sudoku::get(short x,short y) {
      return field[x][y];
   }
   bool Sudoku::get(short x, short y, short z) {
      return possible[x][y][z];
   }
   void Sudoku::del(short x,short y) {
      short z = get(x,y);
      delrow(x,z);
      delcolumn(y,z);
      delgrid(x,y,z);
      delfield(x,y);
   }
   void Sudoku::del(short x,short y,short z) {
      delrow(x,z);
      delcolumn(y,z);
      delgrid(x,y,z);
      delfield(x,y);
   }
   void Sudoku::delrow(short x, short z) {
      for (short j = 0;j < 9;j++) {
          set(x,j,z-1,false); // possibility row false
      }
   }
   void Sudoku::delcolumn(short y, short z) {
      for (short i = 0;i < 9;i++) {
          set(i,y,z-1,false); // possibility column false
      }
   }
   void Sudoku::delgrid(short x, short y, short z) {
      for (short p = 0;p < 3;p++) {
         for (short o = 0;o < 3;o++) {
            set(o+((x/3)*3),p+((y/3)*3),z-1,false); // possibility grid false
         }
      }
   }
   void Sudoku::delfield(short x, short y) {
      for (short l = 0;l < 9;l++) {
         set(x,y,l,false); // possibility field false
      }
   }
