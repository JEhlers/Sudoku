#include "Sudoku.h"
#include "getch.h"

int main( int argc, const char* argv[] ) {
   Sudoku Sudoku1;
   int C = getch();
   if (C == 'I') {
      Sudoku1.input();
   } else {
      Sudoku1.preset(C-48);
   }
   //Sudoku1.input();
   Sudoku1.print();
   Sudoku1.printall();
   Sudoku1.solve();
   Sudoku1.print();
   Sudoku1.printall();
   return 0;
}
